# Teste Comerc
http://www.ddferraz.com.br/homolog/comerc/

## Requerimentos ##
Foi utilizado o Grunt para executar o projeto e Sass com Compass para processar o css.

Necessário Instalar:

1. [Ruby](https://rubyinstaller.org/) - Instalar o [Sass](https://sass-lang.com/) e o [Compass](http://compass-style.org/);
2. [Node](https://nodejs.org/en/download/);
3. [Grunt](https://gruntjs.com/getting-started);

Caso já possua tudo configurado basta instalar as dependências do projeto e iniciar o servidor:

1. `npm install`
2. `grunt serve` para rodar `http://localhost:9000`.

## Documentação ##

### JS

1. `Content/js/lib` - Bibliotecas de JS (No caso jQuery);
2. `Content/js/min` - Arquivo minificado que é chamado no html;
3. `Content/js/plugin` - Plugins de JS necessários para o funcionamento do projeto. (Infinite Scroll e Masonry);
4. `Content/js/src` - Arquivo de customização do JS onde são colocadas as chamadas dos plugins.

### CSS

1. `Content/css`- Arquivo CSS minificado que é chamado no html;
2. `Content/sass/mixis` - Arquivos com os mixins do projeto;
3. `Content/sass/modules` - Arquivos de includes da página (header, content, etc);
4. `Content/sass/page`- Arquivos com o conteúdo específico de cada página;
5. `Content/sass/partials` - Arquivos com configurações gerais do css (variáveis, resets, etc.).

### HTML

1. `index.html` - Html principal;
2. `internal-pages` - Html das páginas que são carregadas pelo inifinite-scroll.