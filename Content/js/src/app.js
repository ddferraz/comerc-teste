var ComercApp = {
    
    init: function(){
        this.funcMasonry();
        this.funcScroll();
    },

    funcMasonry: function(){
        $('.js-grid-masonry').masonry({
            // options
            itemSelector: '.js-masonry-item',
            columnWidth: '.js-masonry-item',
            percentPosition: true
        });
    },

    funcScroll: function(){

        var $scrollMain = $('.js-infinite-scroll'),
            $scrollItem = '.js-item-scroll';

        $scrollMain.infinitescroll({
            navSelector: '.js-scroll-pages',
            nextSelector: '.js-scroll-pages a',
            itemSelector: $scrollItem,
            loading: {
                msgText: 'Page Loader',
                finishedMsg: 'No more pages to load'
            }
        }, function (els) {
            var $loadEls = $(els);
            $scrollMain.masonry('appended', $loadEls, true);

        });
    }

}

ComercApp.init();