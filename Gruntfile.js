module.exports = function(grunt){
    'use strict';

    grunt.initConfig({

        // Package
        pkg: grunt.file.readJSON('package.json'),        

        // uglify
        uglify: {
            options: {
                mangle: false
            },
            my_target: {
                files: {
                    'Content/js/min/app.min.js': ['Content/js/lib/*.js', 'Content/js/plugins/*.js', 'Content/js/src/*.js']
                }
            }
        },

        // Compass
        compass: {
            dist: {
                options: {
                    cssDir: 'Content/css',
                    sassDir: 'Content/sass',
                    imagesDir: 'Content/images',
                    fontsDir: 'Content/fonts',
                    javascriptsDir: 'Content/js',
                    relativeAssets: true,
                    noLineComments: true,
                    boring: true,
                    outputStyle: 'compressed'
                }
            }
        },      

        // Watch Files
        watch: {
            options: {
                livereload: false,
            },
            css: {
                files: [
                    'Content/sass/**/*.scss'
                ],
                tasks: ['compass']
            },
            js: {
                files: [
                    'Content/js/lib/*.js',
                    'Content/js/plugins/*.js',
                    'Content/js/src/*.js'
                ],
                tasks: ['uglify']
            }
        },

        serve: {
            options: {
                port: 9000
            }
        }

    });
 
    // Plugins
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-serve');

    // Taks
    grunt.registerTask('default', 
        [
            'compass',
            'uglify',
            'serve'
        ]
    );
    grunt.registerTask('w', ['watch']);
    
};